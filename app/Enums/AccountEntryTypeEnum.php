<?php

namespace App\Enums;

enum AccountEntryTypeEnum: string
{
    case CREDIT = 'credit';
    case DEBIT = 'debit';

    public static function make(AccountEntryTypeEnum|int $type)
    {
        if (is_a($type, static::class)) {
            return $type;
        }

        return static::from($type);
    }

    public function labels()
    {
        return match ($this) {
            self::CREDIT => 'Credit',
            self::DEBIT => 'Debit',
        };
    }
}
