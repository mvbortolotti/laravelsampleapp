<?php

namespace App\Http\Controllers;

use App\Http\Requests\Cashier\OpenCashierRequest;
use App\Models\Cashier;
use App\Models\Payment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class CashierController extends Controller
{
    public function index(Cashier $cashier)
    {
        if (! $cashier->exists) {
            $cashier = Cashier::getLastOpened();
        }

        $paymentsByType = $cashier
            ->payments()
            ->with(['type', 'table'])
            ->get()
            ->groupBy('payment_type_id')
            ->mapWithKeys(function ($payments) {
                $firstPayment = $payments->first();
                $sum = $payments->sum('value');

                return [$firstPayment->type->name => [
                    'type_id' => $firstPayment->id,
                    'type_name' => $firstPayment->type->name,
                    'sum' => round($sum, 2),
                    'payments' => $payments->map(fn (Payment $payment) => $payment->setAppends(['paid_at', 'table_number'])),
                ]];
            });

        return Inertia::render('Cashier/CashierShow', [
            'cashier' => $cashier->setAppends(['opening_time']),
            'paymentsByType' => $paymentsByType,
        ]);
    }

    public function openForm(): Response|RedirectResponse
    {
        if (Cashier::hasCashierOpen()) {
            return Redirect::route('orders.edit');
        }

        return Inertia::render('Cashier/CashierOpenForm', [
            'cashier' => new Cashier,
        ]);
    }

    public function open(OpenCashierRequest $request): RedirectResponse
    {
        if (Cashier::hasCashierOpen()) {
            return Redirect::route('orders.edit');
        }

        Cashier::doOpen($request->validated('money_amount'));

        return Redirect::route('orders.edit');
    }

    public function close(Cashier $cashier)
    {
        if (! $cashier->exists) {
            $cashier = Cashier::getLastOpened();
        }
        $cashier->doClose();

        return Redirect::route('statement.index');
    }
}
