<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\StoreOrderRequest;
use App\Models\Cashier;
use App\Models\Order;
use App\Models\Table;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class OrdersController extends Controller
{
    public function edit(Order $order): RedirectResponse|Response
    {
        if (! Cashier::hasCashierOpen()) {
            return Redirect::route('cashier.open');
        }

        return Inertia::render('Orders/OrderForm', [
            'order' => $order,
            'editing' => $order->exists,
        ]);
    }

    /**
     * Place new order
     */
    public function store(StoreOrderRequest $request): RedirectResponse
    {
        if (! Cashier::hasCashierOpen()) {
            return Redirect::route('cashier.openForm');
        }

        if (! empty($request->validated('table_id', null))) {
            $realTable = Table::find($request->input('table_id'));
        } else {
            $realTable = Table::getOpenTable($request->input('table'));
        }

        $realTable->placeOrder($request->validated('product_id'), $request->validated('quantity'), $request->has('charge_service_fee') ? true : false);

        return Redirect::back();
    }
}
