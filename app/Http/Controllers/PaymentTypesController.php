<?php

namespace App\Http\Controllers;

use App\Models\PaymentType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PaymentTypesController extends Controller
{
    public function index(Request $request): JsonResponse|Response
    {
        $allowedFilters = [
            AllowedFilter::partial('name'),
        ];

        $paymentTypes = QueryBuilder::for(PaymentType::class)->allowedFilters($allowedFilters);

        if ($request->expectsJson()) {
            return new JsonResponse(['results' => $paymentTypes->get()]);
        }

        return Inertia::render('Products/List', [
            'products' => $paymentTypes->paginate()->appends($request->query()),
            'queryString' => $request->query(),
        ]);
    }
}
