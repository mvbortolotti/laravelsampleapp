<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payments\StorePaymentByPriceRequest;
use App\Models\Table;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class PaymentsController extends Controller
{
    public function storeByPrice(Table $table, StorePaymentByPriceRequest $request): RedirectResponse
    {
        $table->addPaymentByPrice($request->input('payment_type_id'), $request->input('value'));

        return Redirect::back();
    }
}
