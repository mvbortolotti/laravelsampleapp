<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCategories\StoreProductCategoryRequest;
use App\Models\ProductCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductCategoriesController extends Controller
{
    /**
     * List product categories
     */
    public function index(Request $request)
    {
        $allowedFilters = [
            AllowedFilter::partial('name'),
        ];

        $productCategories = QueryBuilder::for(ProductCategory::class)->allowedFilters($allowedFilters);

        return Inertia::render('ProductCategories/ProductCategoriesList', [
            'productCategories' => $productCategories->paginate()->appends($request->query()),
            'queryString' => $request->query(),
        ]);
    }

    /**
     * Display the product categories form.
     */
    public function edit(ProductCategory $productCategory): Response
    {
        return Inertia::render('ProductCategories/ProductCategoryForm', [
            'productCategory' => $productCategory,
            'editing' => $productCategory->exists,
        ]);
    }

    /**
     * Store the product category information.
     */
    public function store(ProductCategory $productCategory, StoreProductCategoryRequest $request): RedirectResponse
    {
        $productCategory->fill($request->validated());
        $productCategory->save();

        return Redirect::back();
    }

    /**
     * Delete the product category and related products.
     */
    public function destroy(Request $request): RedirectResponse
    {
//        return Redirect::to('/');
    }
}
