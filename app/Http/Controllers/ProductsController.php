<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreProductRequest;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductsController extends Controller
{
    /**
     * List products
     */
    public function index(Request $request)
    {
        $allowedFilters = [
            AllowedFilter::partial('name'),
        ];

        $products = QueryBuilder::for(Product::class)->allowedFilters($allowedFilters);

        $product = new Product;
        if ($request->expectsJson()) {
            return new JsonResponse(['results' => $products->limit($product->getPerPage())->get()]);
        } else {
            $products = $products->with('category');
        }

        return Inertia::render('Products/ProductsList', [
            'products' => $products->paginate()->appends($request->query()),
            'queryString' => $request->query(),
        ]);
    }

    /**
     * Display the products form.
     */
    public function edit(Product $product): Response
    {
        return Inertia::render('Products/ProductForm', [
            'product' => $product,
            'editing' => $product->exists,
            'productCategories' => ProductCategory::pluck('name', 'id'),
        ]);
    }

    /**
     * Store the product information.
     */
    public function store(Product $product, StoreProductRequest $request): RedirectResponse
    {
        $data = $request->validated();
        if (! $request->validated('has_pair_discount', false)) {
            $data['price_with_discount'] = null;
        }
        $product->fill($data);
        $product->save();

        return Redirect::back();
    }

    /**
     * Delete the product category and related products.
     */
    public function destroy(Request $request): RedirectResponse
    {
//        return Redirect::to('/');
    }
}
