<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreProductRequest;
use App\Models\Product;
use App\Models\Table;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class TablesController extends Controller
{
    public function summary(Request $request)
    {
        return Inertia::render('Tables/Summary', [
            'tables' => Table::open()->with('payments.type')->orderBy('number')->get()->map(function (Table $table) {
                $table->setAppends(['total', 'service_fee', 'remaining_total']);

                return $table->summarize();
            }),
        ]);
    }

    public function show(Table $table): Response
    {
        return Inertia::render('Tables/Show', [
            'table' => $table
                ->load('payments.type')
                ->setAppends(['subtotal', 'total', 'service_fee', 'remaining_total'])
                ->summarize(),
            'total_people' => 1,
        ]);
    }

    public function store(Product $product, StoreProductRequest $request): RedirectResponse
    {
        $product->fill($request->validated());
        $product->save();

        return Redirect::route('products.edit');
    }
}
