<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'table' => ['nullable', 'required_without:table_id', 'numeric'],
            'table_id' => ['nullable', 'required_without:table', 'numeric'],
            'product_id' => ['required', 'exists:products,id'],
            'quantity' => ['required', 'numeric', 'gte:1'],
            'charge_service_fee' => ['sometimes', 'boolean'],
        ];
    }
}
