<?php

namespace App\Http\Requests\Products;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255', Rule::unique(Product::class, 'name')->ignoreModel($this->route('product') ?? new Product)],
            'product_category_id' => ['required', 'exists:product_categories,id'],
            'price' => ['required', 'numeric', 'gt:0'],
            'has_pair_discount' => ['sometimes', 'boolean'],
            'price_with_discount' => [
                'sometimes',
                'exclude_if:has_pair_discount,false',
                'required_if:has_pair_discount,true',
                'numeric',
                'gt:0',
                'lt:'.$this->input('price'),
            ],
        ];
    }
}
