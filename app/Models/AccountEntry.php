<?php

namespace App\Models;

use App\Enums\AccountEntryTypeEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AccountEntry extends Model
{
    use HasFactory;

    protected $fillable = ['description', 'type', 'value', 'payment_id'];

    protected $casts = [
        'type' => AccountEntryTypeEnum::class,
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public static function getMonthList(): Collection
    {
        $months = DB::select('select DISTINCT MONTH(created_at) as month, YEAR(created_at) as year from account_entries order by MONTH(created_at), YEAR(created_at);');

        return collect($months)->mapWithKeys(function ($val) {
            return [$val->year.'-'.$val->month => $val->month.'/'.$val->year];
        });
    }

    public function scopeFromMonth(Builder $query, Carbon $month): void
    {
        $startOfMonth = (clone $month)->startOfMonth();
        $endOfMonth = (clone $month)->endOfMonth();

        $query->whereBetween('created_at', [$startOfMonth, $endOfMonth]);
    }

    public function isCredit(): bool
    {
        return AccountEntryTypeEnum::CREDIT()->equals($this->type);
    }

    public function isDebit(): bool
    {
        return AccountEntryTypeEnum::DEBIT()->equals($this->type);
    }

    public static function getBalanceBefore(Carbon $month): float
    {
        $startOfMonth = $month->startOfMonth();

        return static::where('created_at', '<=', $startOfMonth)->sum('value');
    }

    private function formattedCreatedAt(): Attribute
    {
        return new Attribute(get: fn () => $this->created_at->format('d/m/Y H:i:s'));
    }

    public function formattedValue(): Attribute
    {
        return new Attribute(get: fn () => 'R$ '.number_format($this->value, 2, ',', '.'));
    }

    public static function createDebit($description, $amount)
    {
        return static::create([
            'description' => $description,
            'type' => AccountEntryTypeEnum::DEBIT,
            'value' => -(abs($amount)),
        ]);
    }

    public static function createCredit(Payment $payment)
    {
        return static::create([
            'description' => "Pagamento de conta ({$payment->type->name})",
            'type' => AccountEntryTypeEnum::CREDIT,
            'value' => $payment->value,
            'payment_id' => $payment->id,
        ]);
    }
}
