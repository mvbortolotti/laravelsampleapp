<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Str;

class Cashier extends Model
{
    use HasFactory;

    protected $fillable = ['day', 'is_open', 'opening_amount', 'closing_amount', 'summary'];

    protected $casts = [
        'summary' => 'json',
        'is_open' => 'boolean',
        'opening_amount' => 'float',
        'day' => 'datetime',
    ];

    /**
     * Relationships
     */
    public function tables(): HasMany
    {
        return $this->hasMany(Table::class, 'cashier_id');
    }

    public function orders(): HasManyThrough
    {
        return $this->hasManyThrough(Order::class, Table::class);
    }

    public function payments(): HasManyThrough
    {
        return $this->hasManyThrough(Payment::class, Table::class);
    }

    /**
     * Filter opened cashiers
     */
    public function scopeOpen(Builder $query): void
    {
        $query->where('is_open', true);
    }

    /**
     * Utilitary methods
     */
    public static function getLastOpened(): static
    {
        return static::latest('id')->open()->first();
    }

    public static function getOpenId(): int
    {
        return static::getLastOpened()->id;
    }

    public static function hasCashierOpen(): bool
    {
        return static::open()->exists();
    }

    protected function openingTime(): Attribute
    {
        return Attribute::make(get: fn () => $this->day->format('d/m/Y H:i'));
    }

    public static function doOpen(float $moneyAmount): static
    {
        return Cashier::create([
            'day' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_open' => true,
            'opening_amount' => $moneyAmount,
            'closing_amount' => null,
            'summary' => null,
        ]);
    }

    public function doClose(): void
    {
        if (! $this->canBeClosed()) {
            throw new \Exception('Cannot close Cashier');
        }

        $this->summarize();

        $this->update([
            'closing_amount' => $this->opening_amount + (array_key_exists('cash', $this->summary['payments']) ? $this->summary['payments']['cash']['total_value'] : 0),
            'is_open' => false,
        ]);
    }

    public function canBeClosed(): bool
    {
        return $this->is_open && ! $this->hasOpenTables();
    }

    public function hasOpenTables(): bool
    {
        return $this->tables()->open()->exists();
    }

    public function summarize(): void
    {
        $this->fill([
            'summary' => [
                'payments' => $this->payments->groupBy('payment_type_id')->mapWithKeys(function ($paymentsByType) {
                    return [
                        Str::slug($paymentsByType->first()->type->name) => [
                            'total_value' => $paymentsByType->sum('value'),
                            'items' => $paymentsByType->map(function ($payment) {
                                return [
                                    'created_at' => $payment->created_at->format('d/m/Y H:i:s'),
                                    'value' => number_format($payment->value, 2, ',', '.'),
                                ];
                            }),
                        ],
                    ];
                }),
                'orders' => $this->orders->groupBy('product_id')->mapWithKeys(function ($ordersByProduct) {
                    $product = $ordersByProduct->first()->product;

                    return [
                        $product->id => [
                            'product_name' => $product->name,
                            'quantity' => $ordersByProduct->sum('quantity'),
                        ],
                    ];
                }),
            ],
        ]);

        $this->save();
    }
}
