<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['table_id', 'product_id', 'quantity', 'charge_service_fee'];

    protected $casts = [
        'charge_service_fee' => 'boolean',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function table(): BelongsTo
    {
        return $this->belongsTo(Table::class);
    }

    public static function placeOrder(Table $table, $productId, $quantity, $chargeServiceFee = true): static
    {
        return static::create([
            'table_id' => $table->id,
            'product_id' => $productId,
            'quantity' => $quantity,
            'charge_service_fee' => $chargeServiceFee,
        ]);
    }
}
