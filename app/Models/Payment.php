<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['value', 'table_id', 'payment_type_id', 'sale_id', 'summary'];

    protected $casts = [
        'summary' => 'json',
    ];

    protected function paidAt(): Attribute
    {
        return Attribute::make(get: fn () => $this->created_at->format('d/m/Y H:i:s'));
    }

    protected function tableNumber(): Attribute
    {
        return Attribute::make(get: fn () => $this->table()->first()->number);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    public function sale(): BelongsTo
    {
        return $this->belongsTo(Sale::class);
    }

    public function table(): BelongsTo
    {
        return $this->belongsTo(Table::class);
    }
}
