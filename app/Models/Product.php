<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'price', 'has_pair_discount', 'price_with_discount', 'product_category_id'];

    protected $appends = ['formatted_price'];

    protected $casts = [
        'has_pair_discount' => 'bool',
        'price' => 'float',
        'price_with_discount' => 'float',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    protected function categoryName(): Attribute
    {
        return new Attribute(get: fn () => $this->category->name);
    }

    protected function formattedPrice(): Attribute
    {
        return Attribute::make(get: function () {
            return 'R$ '.number_format($this->price, 2, ',', '.');
        });
    }
}
