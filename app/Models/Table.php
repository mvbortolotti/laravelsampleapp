<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Table extends Model
{
    use HasFactory, SoftDeletes, HasSlug;

    protected $fillable = ['number', 'slug', 'summary', 'cashier_id', 'is_open'];

    protected $casts = [
        'summary' => 'json',
        'is_open' => 'bool',
    ];

    public function cashier(): BelongsTo
    {
        return $this->belongsTo(Cashier::class);
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class);
    }

    public function paymentsByPrice(): HasMany
    {
        return $this->payments()->whereNull('summary');
    }

    public function paymentsByItems(): HasMany
    {
        return $this->payments()->whereNotNull('summary');
    }

    public function scopeOpen($query)
    {
        return $query
            ->whereHas('cashier', function ($cashierQuery) {
                return $cashierQuery->open();
            })
            ->where('is_open', true);
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['cashier_id', 'number'])
            ->saveSlugsTo('slug')
            ->usingLanguage('pt_BR');
    }

    protected function serviceFee(): Attribute
    {
        return new Attribute(get: function () {
            return round(
                collect($this->summary)
                    ->where('charge_service_fee', true)
                    ->sum('total_price') * env('SERVICE_FEE', 0.1),
                2
            );
        });
    }

    protected function subtotal(): Attribute
    {
        return new Attribute(get: fn () => round(collect($this->summary)->sum('total_price'), 2));
    }

    protected function total(): Attribute
    {
        return new Attribute(get: fn () => round($this->subtotal + $this->service_fee, 2));
    }

    protected function remainingTotal(): Attribute
    {
        return new Attribute(get: function () {
            $remaining = $this->total - $this->payments()->sum('value');
            if ($remaining < 0.05) {
                return 0;
            }

            return round($remaining, 2);
        });
    }

    public static function getOpenTable($tableNumber)
    {
        return static::firstOrCreate([
            'number' => $tableNumber,
            'cashier_id' => Cashier::getOpenId(),
            'is_open' => true,
        ]);
    }

    public function placeOrder($productId, $quantity, $chargeServiceFee = true)
    {
        $order = Order::placeOrder($this, $productId, $quantity, $chargeServiceFee);

        $this->summarize();

        return $order;
    }

    public function summarize(): static
    {
        $summary = [];

        $paymentsByItems = $this
            ->paymentsByItems()
            ->get()
            ->pluck('summary')
            ->reduce(function ($carry, $item) {
                return $carry->concat($item);
            }, collect([]))
            ->groupBy(function ($item) {
                return $item['product_id'].'|'.$item['charge_service_fee'];
            })->map(function ($items) {
                return $items->sum('quantity');
            });

        $this
            ->orders()
            ->with('product')
            ->get()
            ->sortBy('product_id')
            ->groupBy('product_id')
            ->map(function ($ordersByProduct, $productId) use (&$summary, $paymentsByItems) {
                $orders = $ordersByProduct->groupBy('charge_service_fee');
                foreach ($orders as $ordersByProduct) {
                    $product = $ordersByProduct->first()->product;
                    $firstOrder = $ordersByProduct->first();
                    $productsQuantity = $ordersByProduct->sum('quantity');
                    $pairs = floor($ordersByProduct->sum('quantity') / 2);
                    $paidQuantity = $paymentsByItems->get($product->id.'|'.($firstOrder->charge_service_fee ? 1 : 0), 0);

                    if ($product->has_pair_discount && $pairs > 0) {
                        $single = $productsQuantity % 2;

                        $summary[] = [
                            'product_id' => $product->id,
                            'product_name' => $product->name,
                            'detailedQuantity' => [
                                'single' => $single,
                                'pairs' => $pairs,
                            ],
                            'quantity' => $productsQuantity,
                            'paid_quantity' => $paidQuantity,
                            'remaining_quantity' => $productsQuantity - $paidQuantity,
                            'charge_service_fee' => $firstOrder->charge_service_fee,
                            'unit_price' => $product->price_with_discount,
                            'total_price' => round($productsQuantity * $product->price_with_discount, 2),
                        ];
                    } else {
                        $summary[] = [
                            'product_id' => $product->id,
                            'product_name' => $product->name,
                            'quantity' => $productsQuantity,
                            'paid_quantity' => $paidQuantity,
                            'remaining_quantity' => $productsQuantity - $paidQuantity,
                            'charge_service_fee' => $ordersByProduct->first()->charge_service_fee,
                            'unit_price' => $product->price,
                            'total_price' => round($productsQuantity * $product->price, 2),
                        ];
                    }
                }
            });

        $this->update([
            'summary' => $summary,
        ]);

        return $this;
    }

    public function addPaymentByPrice($paymentTypeId, $price): ?Payment
    {
        try {
            DB::beginTransaction();
            $payment = Payment::create([
                'value' => $price,
                'table_id' => $this->id,
                'payment_type_id' => $paymentTypeId,
                'sale_id' => null,
                'summary' => null,
            ]);

            AccountEntry::createCredit($payment);

            $this->closeIfAllPaid();

            DB::commit();

            return $payment;
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function addPaymentByItems($items, $paymentTypeId, $price)
    {
        try {
            DB::beginTransaction();
            $items = collect($items)->where('quantity', '>', 0);
            $payment = Payment::create([
                'value' => $price,
                'table_id' => $this->id,
                'payment_type_id' => $paymentTypeId,
                'sale_id' => null,
                'summary' => $items->values(),
            ]);

            AccountEntry::createCredit($payment);

            $this->closeIfAllPaid();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    private function closeIfAllPaid()
    {
        if ($this->remaining_total == 0) {
            $this->update(['is_open' => false]);
        }
    }
}
