<?php

namespace App\Providers;

use App\Models\Cashier;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Inertia::share('hasCashierOpen', fn () => Cashier::hasCashierOpen());
    }
}
