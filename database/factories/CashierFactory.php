<?php

namespace Database\Factories;

use App\Models\Cashier;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CashierFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cashier::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'day' => Carbon::now()->format('Y-m-d'),
            'is_open' => true,
            'opening_amount' => 0,
            'closing_amount' => null,
            'summary' => null,
        ];
    }
}
