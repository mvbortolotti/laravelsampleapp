<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use App\Models\Table;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'quantity' => 1,
            'charge_service_fee' => 1,
        ];
    }

    public function withTable(Table $table = null): static
    {
        if (is_null($table)) {
            $table = Table::factory()->withOpenCashier();
        }

        return $this->for($table, 'table');
    }

    public function withProduct(Product $product = null): static
    {
        if (is_null($product)) {
            $product = Product::factory()->withProductCategory();
        }

        return $this->for($product, 'product');
    }
}
