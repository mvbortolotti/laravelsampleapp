<?php

namespace Database\Factories;

use App\Models\Payment;
use App\Models\Table;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'value' => '',
            'table_id' => '',
            'payment_type_id' => '',
            'sale_id' => null,
            'summary' => null,
        ];
    }

    public function forTable(Table $table): static
    {
        return $this->has($table, 'table');
    }
}
