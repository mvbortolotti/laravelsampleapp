<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $price = $this->faker->randomFloat(2, 5, 100);

        return [
            'name' => $this->faker->unique()->numerify('Product ####'),
            'price' => $price,
            'has_pair_discount' => false,
            'price_with_discount' => null,
        ];
    }

    public function withProductCategory(ProductCategory $productCategory = null): static
    {
        if (is_null($productCategory)) {
            $productCategory = ProductCategory::factory();
        }

        return $this->for($productCategory, 'category');
    }

    public function withPairDiscount(): static
    {
        return $this->state(function (array $attributes) {
            return [
                'has_pair_discount' => 1,
                'price_with_discount' => round($attributes['price'] / 2, 2),
            ];
        });
    }
}
