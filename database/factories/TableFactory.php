<?php

namespace Database\Factories;

use App\Models\Cashier;
use App\Models\Table;
use Illuminate\Database\Eloquent\Factories\Factory;

class TableFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Table::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => $this->faker->unique()->numberBetween(0, 20),
            'summary' => [],
            'is_open' => true,
        ];
    }

    public function withOpenCashier(Cashier $cashier = null): static
    {
        if (is_null($cashier)) {
            $cashier = Cashier::factory();
        }

        return $this->for($cashier, 'cashier');
    }
}
