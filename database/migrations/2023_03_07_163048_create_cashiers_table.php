<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cashiers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->dateTime('day');
            $table->boolean('is_open')->default(false);
            $table->decimal('opening_amount', 8, 2);
            $table->decimal('closing_amount', 8, 2)->nullable()->default(null);
            $table->json('summary')->nullable()->default(null);
        });

        Schema::table('tables', function (Blueprint $table) {
            $table->foreignId('cashier_id')->references('id')->on('cashiers');
            $table->boolean('is_open')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('table', function (Blueprint $table) {
            $table->dropForeign(['cashier_id']);
            $table->dropColumn('is_open');
        });
        Schema::dropIfExists('cashiers');
    }
};
