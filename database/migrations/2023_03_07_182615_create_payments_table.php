<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_types', function (Blueprint $table) {
            $table->id('id');
            $table->timestamps();
            $table->softDeletes();

            $table->string('name');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->decimal('value', 8, 2);
            $table->foreignId('table_id')->references('id')->on('tables');
            $table->foreignId('payment_type_id')->references('id')->on('payment_types');
            $table->foreignId('sale_id')->nullable()->default(null)->references('id')->on('sales');

            $table->json('summary')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
        Schema::dropIfExists('payment_types');
    }
};
