<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypesSeeder extends Seeder
{
    protected static $types = [
        'Cash',
        'Crédit Card',
        'Débit Card',
        'PIX',
        'Discount',
    ];

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        foreach (static::$types as $type) {
            PaymentType::create(['name' => $type]);
        }
    }
}
