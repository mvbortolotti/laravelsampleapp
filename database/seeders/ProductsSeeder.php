<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    protected static $menu = [
        'Cervejas' => [
            'Original 600ml' => 10.9,
            'Amstel 600ml' => 10.9,
            'Cacildis 600ml' => 10.9,
            'Heineken 600ml' => 14.3,
            'Itaipava Premium 600ml' => 8.9,
            'Petra Puro Malte 600ml' => 9.9,
            'Skol 300ml' => [
                'price' => 4.25,
                'has_pair_discount' => true,
                'price_with_discount' => 3.95,
            ],
            'Brahma 300ml' => [
                'price' => 4.25,
                'has_pair_discount' => true,
                'price_with_discount' => 3.95,
            ],
            'Itaipava 300ml' => [
                'price' => 3.9,
                'has_pair_discount' => true,
                'price_with_discount' => 3.7,
            ],
            'Sem álcool' => 4.5,
            'Malzebier Long Neck' => 6.5,
            'Stella Artois 550 ml' => 13.5,
            'Eisenbahn 600ml' => 10.5,
        ],
        'Refrigerantes e Águas' => [
            'Água sem gás' => 2.75,
            'Água com gás' => 3.8,
            'Água tônica' => 5.25,
            'Citrus lata' => 5.25,
            'Refri lata (normal e zero)' => 4.9,
            'Coca-cola 600ml' => 7.9,
        ],
        'Destilados' => [
            'Vodka Smirnoff' => 11.5,
            'Vodka Orloff' => 10.9,
            'Cachaça Ypioca Ouro' => 7.9,
            'Saquê' => 10.9,
            'Gin' => 9.9,
            'Rum' => 9.9,
            'Domeq' => 10.9,
            'Steinhaeger' => 10.9,
            'Campari' => 11.9,
            'Red Label' => 17.9,
        ],
        'Vinhos' => [
            'Taça de Vinho Nacional (normalzinho)' => 6.9,
            'Garrafa de Vinho Nacional (normalzinho)' => 16.9,
            'Taça de Vinho Nacional (melhorzinho)' => 8.9,
            'Garrafa de Vinho Nacional (melhorzinho)' => 22.9,
            'Garrafa de Vinho Nacional (do bom)' => 38.9,
            'Garrafa de Vinho Chileno' => 49.9,
        ],
        'Caipirinhas' => [
            'Caipirinha com pinga - Limão' => 9.9,
            'Caipirinha com pinga - Uma fruta' => 12.9,
            'Caipirinha com cachaça - Limão' => 12.9,
            'Caipirinha com cachaça - Uma fruta' => 15.9,
            'Caipirinha com Vodka Tri-destilada - Limão' => 12.9,
            'Caipirinha com Vodka Tri-destilada - Uma fruta' => 15.9,
            'Caipirinha com Orloff ou Smirnoff - Limão' => 15.9,
            'Caipirinha com Orloff ou Smirnoff - Uma fruta' => 17.9,
            'Caipirinha com Saque - Limão' => 14.9,
            'Caipirinha com Saque - Uma fruta' => 16.9,
        ],
        'Spaguetti' => [
            'Spaguetti Ao Sugo (Meia)' => 15.9,
            'Spaguetti Ao Sugo (Inteira)' => 22.9,

            'Spaguetti Bolonhesa (Meia)' => 21.9,
            'Spaguetti Bolonhesa (Inteira)' => 32.9,

            'Spaguetti Bechamel (Meia)' => 15.9,
            'Spaguetti Bechamel (Inteira)' => 22.9,

            'Spaguetti ao Bechamel com Brócolis (Meia)' => 19.9,
            'Spaguetti ao Bechamel com Brócolis (Inteira)' => 29.9,

            'Spaguetti Da Casa (Meia)' => 24.9,
            'Spaguetti Da Casa (Inteira)' => 38.9,

            'Spaguetti Misto Bechamel e Sugo (Meia)' => 19.9,
            'Spaguetti Misto Bechamel e Sugo (Inteira)' => 24.9,

            'Spaguetti Misto Bechamel e Bolonhesa (Meia)' => 22.9,
            'Spaguetti Misto Bechamel e Bolonhesa (Inteira)' => 32.9,

            'Spaguetti Misto Bechamel c/ brócolis e Bolonhesa (Meia)' => 26.9,
            'Spaguetti Misto Bechamel c/ brócolis e Bolonhesa (Inteira)' => 37.9,

            'Spaguetti Misto Bechamel com Brócolis e Sugo (Meia)' => 25.9,
            'Spaguetti Misto Bechamel com Brócolis e Sugo (Inteira)' => 31.9,

            'Spaguetti Misto Da Casa com carinho (Meia)' => 28.9,
            'Spaguetti Misto Da Casa com carinho (Inteira)' => 41.9,
        ],
        'Panquecas' => [
            'Panqueca de Carne com Mussarela' => 25.9,
            'Panqueca de Frando com Requeijão' => 23.9,
            'Panqueca de Presunto e Queijo' => 21.9,
            'Panqueca de Brócolis, cenoura e champignon' => 20.9,
            'Panqueca - Kit acompanhamento' => 13.9,
        ],
        'Extra' => [
            'Arroz' => 6.9,
            'Bacon' => 3.95,
            'Pão' => 1.5,
            'Queijo' => 3.9,
            'Salada de tomate' => 13.9,
            'Salada mista' => 19.9,
            'CDB' => 2.5,
        ],
        'Sucos' => [
            'Suco de Abacaxi (Água)' => 6,
            'Suco de Abacaxi (Leite)' => 6.75,
            'Suco de Abacaxi (Laranja)' => 7.5,

            'Suco de Abacaxi com Hortelã (Água)' => 6.25,
            'Suco de Abacaxi com Hortelã (Leite)' => 6.95,
            'Suco de Abacaxi com Hortelã (Laranja)' => 7.75,

            'Suco de Limão (Água)' => 4.9,
            'Suco de Limão (Laranja)' => 5.2,

            'Suco de Limão com Hortelã (Água)' => 5.9,
            'Suco de Limão com Hortelã (Laranja)' => 6.4,

            'Suco de Laranja' => 5.9,

            'Suco de Maracujá (Água)' => 7.3,
            'Suco de Maracujá (Leite)' => 8,
            'Suco de Maracujá (Laranja)' => 7.6,

            'Suco de Morango (Água)' => 7.75,
            'Suco de Morango (Leite)' => 8.5,
            'Suco de Morango (Laranja)' => 8,

            'Suco de Kiwi (Água)' => 7.75,
            'Suco de Kiwi (Leite)' => 7.5,
            'Suco de Kiwi (Laranja)' => 8,

            'Suco de Duas frutas (Água)' => 9.9,
            'Suco de Duas frutas (Leite)' => 10.6,
            'Suco de Duas frutas (Laranja)' => 10,
        ],
        'Sucos Especiais' => [
            'Suco de Abacaxi, Gengibre e Laranja' => 10.5,
            'Suco de Laranja e Cenouora' => 9.9,
            'Suco de Couve, Maracujá e Limão' => 10.5,
            'Suco de Limão cremoso' => 11.5,
            'Suco de Amendoim' => 11.5,
            'Suco de Jarra Laranja (700ml)' => 11.9,
            'Suco - Adicional de leite condensado' => 1,
        ],
        'Batidas' => [
            'Batida de Vinho' => 11.9,
            'Batida de Vinho com fruta' => 15.9,
            'Batida de Vodka Tri-Destilada' => 13.9,
            'Batida de Vodka Orloff ou Smirnoff' => 16.9,
        ],
        'Porções' => [
            'Porção de Azeitonas (Meia)' => 7.9,
            'Porção de Azeitonas (Inteira)' => 12.8,

            'Porção de Salame (Meia)' => 16.95,
            'Porção de Salame (Inteira)' => 29.9,

            'Porção de Presunto e Queijo (Meia)' => 17.9,
            'Porção de Presunto e Queijo (Inteira)' => 28.9,

            'Tábua de frios (Meia)' => 23.9,
            'Tábua de frios (Inteira)' => 41.9,

            'Porção de Mandioca (Meia)' => 14.9,
            'Porção de Mandioca (Inteira)' => 22.9,

            'Porção de Mandioca com Queijo (Meia)' => 17.9,
            'Porção de Mandioca com Queijo (Inteira)' => 27.9,

            'Porção de Mandioca com Bacon (Meia)' => 17.9,
            'Porção de Mandioca com Bacon (Inteira)' => 27.9,

            'Porção de Mandioca com Queijo e Bacon (Meia)' => 19.9,
            'Porção de Mandioca com Queijo e Bacon (Inteira)' => 29.9,

            'Porção de Batata (Meia)' => 13.9,
            'Porção de Batata (Inteira)' => 21.9,

            'Porção de Batata com Queijo (Meia)' => 15.9,
            'Porção de Batata com Queijo (Inteira)' => 24.9,

            'Porção de Batata com Bacon (Meia)' => 15.9,
            'Porção de Batata com Bacon (Inteira)' => 24.9,

            'Porção de Batata com Queijo e Bacon (Meia)' => 18.9,
            'Porção de Batata com Queijo e Bacon (Inteira)' => 28.9,

            'Amendoim (pacotinho)' => 2.7,

            'Porção de Isca de Frango (Meia)' => 16.9,
            'Porção de Isca de Frango (Inteira)' => 27.9,

            'Porção de Frango Frito(Meia)' => 16.9,
            'Porção de Frango Frito(Inteira)' => 27.9,

            'Porção de Tirinha de Pernil Empanada (Meia)' => 16.9,
            'Porção de Tirinha de Pernil Empanada (Inteira)' => 27.9,

            'Porção de Calabresa Acebolada (Meia)' => 15.9,
            'Porção de Calabresa Acebolada (Inteira)' => 25.9,

            'Porção de Torresmo (Meia)' => 17.95,
            'Porção de Torresmo (Inteira)' => 31.9,

            'Porção de Isca de Tilápia (Meia)' => 27.9,
            'Porção de Isca de Tilápia (Inteira)' => 43.9,
        ],
        'Porções com acompanhamento' => [
            'Porção de Contra filé acebolado (Meia)' => 31.9,
            'Porção de Contra filé acebolado (Inteira)' => 57.9,

            'Porção de Carne Seca (Meia)' => 28.9,
            'Porção de Carne Seca (Inteira)' => 49.9,

            'Porção de Filé de frango na chapa (Meia)' => 25.9,
            'Porção de Filé de frango na chapa (Inteira)' => 37.9,

            'Porção de Pernil Chinês (Meia)' => 26.9,
            'Porção de Pernil Chinês (Inteira)' => 41.9,

            'Porção de Costelinha de Porco (Meia)' => 28.9,
            'Porção de Costelinha de Porco (Inteira)' => 48.9,
        ],
        'Omeletes' => [
            'Omelete de Queijo' => 14.9,
            'Omelete de Presunto e Queijo' => 16.9,
            'Omelete de Brócolis e Champignons' => 15.9,
            'Omelete de Completo' => 18.9,
            'Omelete de Da casa' => 19.9,
        ],
        'Parmegianas' => [
            'Parmegiana de Bife (Meia)' => 33.9,
            'Parmegiana de Bife (Inteira)' => 55.9,
            'Parmegiana Individual de Bife' => 21.9,
            'Parmegiana de Isca de Frango (Meia)' => 29.9,
            'Parmegiana de Isca de Frango (Inteira)' => 44.9,
            'Parmegiana de Tilápia (Meia)' => 35.9,
            'Parmegiana de Tilápia (Inteira)' => 59.9,
        ],
    ];

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        foreach (static::$menu as $category => $products) {
            $category = ProductCategory::firstOrCreate(['name' => $category]);
            foreach ($products as $name => $price) {
                if (! is_array($price)) {
                    $price = ['price' => $price];
                }
                $category->products()->create(array_merge(['name' => $name], $price));
            }
        }
    }
}
