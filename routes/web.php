<?php

use App\Http\Controllers\CashierController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\PaymentTypesController;
use App\Http\Controllers\ProductCategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TablesController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::middleware(['auth', 'verified'])->name('cashier.')->prefix('/cashier')->group(function () {
    Route::get('/', [CashierController::class, 'index'])->name('index');
    Route::get('/open', [CashierController::class, 'openForm'])->name('openForm');
    Route::post('/open', [CashierController::class, 'open'])->name('open');
    Route::post('/close', [CashierController::class, 'close'])->name('close');
});

Route::middleware(['auth', 'verified'])->name('statement.')->prefix('/statement')->group(function () {
    Route::get('/', [CashierController::class, 'index'])->name('index');
});

Route::middleware(['auth', 'verified'])->name('tables.')->prefix('/tables')->group(function () {
    Route::get('/', [TablesController::class, 'summary'])->name('summary');
    Route::get('/{table?}', [TablesController::class, 'show'])->name('show');
    Route::get('/edit/{table?}', [TablesController::class, 'edit'])->name('edit');
    Route::post('/{table?}', [TablesController::class, 'store'])->name('store');
});

Route::middleware(['auth', 'verified'])->name('orders.')->prefix('/orders')->group(function () {
    Route::get('/{order?}', [OrdersController::class, 'edit'])->name('edit');
    Route::post('/{order?}', [OrdersController::class, 'store'])->name('store');
});

Route::middleware(['auth', 'verified'])->name('product-categories.')->prefix('/product-categories')->group(function () {
    Route::get('/', [ProductCategoriesController::class, 'index'])->name('index');
    Route::get('/edit/{productCategory?}', [ProductCategoriesController::class, 'edit'])->name('edit');
    Route::post('/{productCategory?}', [ProductCategoriesController::class, 'store'])->name('store');
});

Route::middleware(['auth', 'verified'])->name('payment-types.')->prefix('/payment-types')->group(function () {
    Route::get('/', [PaymentTypesController::class, 'index'])->name('index');
});

Route::middleware(['auth', 'verified'])->name('payments.')->prefix('/payments')->group(function () {
    Route::post('/by-price/{table}', [PaymentsController::class, 'storeByPrice'])->name('storeByPrice');
});

Route::middleware(['auth', 'verified'])->name('products.')->prefix('/products')->group(function () {
    Route::get('/', [ProductsController::class, 'index'])->name('index');
    Route::get('/edit/{product?}', [ProductsController::class, 'edit'])->name('edit');
    Route::post('/{product?}', [ProductsController::class, 'store'])->name('store');
    Route::get('remove/{product}', [ProductsController::class, 'destroy'])->name('destroy');
});
