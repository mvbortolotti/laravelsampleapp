<?php

namespace Tests\Feature\Cashier;

use App\Models\Cashier;
use App\Models\Order;
use App\Models\PaymentType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CashierTest extends TestCase
{
    use RefreshDatabase;

    public function test_cashier_can_be_opened(): void
    {
        $this->actingAs($user = User::factory()->create());

        $response = $this->post(route('cashier.open'), [
            'money_amount' => 0,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Cashier::open()->get());
        $this->assertTrue(Cashier::latest('id')->first()->is_open);
        $this->assertEquals(0, Cashier::latest('id')->first()->opening_amount);
    }

    public function test_cashier_cant_be_opened_while_another_is_opened(): void
    {
        $this->actingAs($user = User::factory()->create());

        $cashier = Cashier::factory()->create();
        $response = $this->post(route('cashier.open'), [
            'money_amount' => 0,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Cashier::open()->get());
        $this->assertEquals($cashier->id, Cashier::latest('id')->first()->id);
    }

    public function test_cashier_can_be_closed(): void
    {
        $this->actingAs($user = User::factory()->create());

        $cashier = Cashier::factory()->create();
        $response = $this->post(route('cashier.close', ['cashier' => $cashier]));
        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $cashier->refresh();
        $this->assertCount(0, Cashier::open()->get());
        $this->assertFalse($cashier->is_open);
        $this->assertEquals(0, Cashier::latest('id')->first()->opening_amount);
        $this->assertEquals(0, Cashier::latest('id')->first()->closing_amount);

        $this->assertEquals($cashier->summary, [
            'payments' => [],
            'orders' => [],
        ]);
    }

    public function test_cashier_cant_be_closed_with_open_tables(): void
    {
        $this->actingAs($user = User::factory()->create());

        $order = Order::factory()->withTable()->withProduct()->create();
        $cashier = $order->table->cashier;

        $this->assertFalse($cashier->canBeClosed());

        $response = $this->post(route('cashier.close', ['cashier' => $cashier]));
        $response->assertServerError();
    }

    public function test_cashier_summary(): void
    {
        $this->actingAs($user = User::factory()->create());

        $order = Order::factory()->withTable()->withProduct()->create();
        $cashier = $order->table->cashier;
        $product = $order->product;
        $totalPrice = round($product->price + ($product->price * env('SERVICE_FEE', 0.1)), 2);

        $this->assertFalse($cashier->canBeClosed());

        $payment = $order->table->addPaymentByPrice(PaymentType::factory()->create(['name' => 'cash'])->id, $totalPrice);

        $this->assertTrue($cashier->canBeClosed());
        $response = $this->post(route('cashier.close', ['cashier' => $cashier]));
        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();
        $cashier->refresh();
        $this->assertCount(0, Cashier::open()->get());
        $this->assertFalse($cashier->is_open);
        $this->assertEquals(0, Cashier::latest('id')->first()->opening_amount);
        $this->assertEquals($totalPrice, Cashier::latest('id')->first()->closing_amount);

        $this->assertEquals($cashier->summary, [
            'payments' => [
                'cash' => [
                    'total_value' => $totalPrice,
                    'items' => [
                        [
                            'created_at' => $payment->created_at->format('d/m/Y H:i:s'),
                            'value' => number_format($totalPrice, 2, ',', '.'),
                        ],
                    ],
                ],
            ],
            'orders' => [
                $product->id => [
                    'product_name' => $product->name,
                    'quantity' => 1,
                ],
            ],
        ]);
    }
}
