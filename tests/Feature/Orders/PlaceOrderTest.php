<?php

namespace Tests\Feature\Orders;

use App\Models\Order;
use App\Models\Product;
use App\Models\Table;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaceOrderTest extends TestCase
{
    use RefreshDatabase;

    public function test_place_order_without_open_cashier(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()->withProductCategory()->create();
        $quantity = 1;
        $response = $this->post(route('orders.store'), [
            'table' => '1',
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect(route('cashier.openForm', [], false));
    }

    public function test_place_order_with_table_number(): void
    {
        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $quantity = 1;
        $response = $this->post(route('orders.store'), [
            'table' => $table->number,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $order = Order::latest('id')->first();
        $this->assertCount(1, Order::get());
        $this->assertEquals($table->id, $order->table_id);

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price,
                'total_price' => round($quantity * $product->price, 2),
            ],
        ]);
    }

    public function test_place_order_with_table_number_and_table_id_null(): void
    {
        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $quantity = 1;
        $response = $this->post(route('orders.store'), [
            'table' => $table->number,
            'table_id' => null,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $order = Order::latest('id')->first();
        $this->assertCount(1, Order::get());
        $this->assertEquals($table->id, $order->table_id);

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price,
                'total_price' => round($quantity * $product->price, 2),
            ],
        ]);
    }

    public function test_place_order_with_table_id(): void
    {
        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $quantity = 1;
        $response = $this->post(route('orders.store'), [
            'table_id' => $table->id,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $order = Order::latest('id')->first();
        $this->assertCount(1, Order::get());
        $this->assertEquals($table->id, $order->table_id);

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price,
                'total_price' => round($quantity * $product->price, 2),
            ],
        ]);
    }

    public function test_place_order_with_table_id_and_table_null(): void
    {
        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $quantity = 1;
        $response = $this->post(route('orders.store'), [
            'table' => null,
            'table_id' => $table->id,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $order = Order::latest('id')->first();
        $this->assertCount(1, Order::get());
        $this->assertEquals($table->id, $order->table_id);

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price,
                'total_price' => round($quantity * $product->price, 2),
            ],
        ]);
    }

    public function test_place_order_with_pair_discount(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()->withProductCategory()->withPairDiscount()->create();
        $table = Table::factory()->withOpenCashier()->create();
        $quantity = 2;
        $response = $this->post(route('orders.store'), [
            'table_id' => $table->id,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Order::get());

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'detailedQuantity' => [
                    'single' => 0,
                    'pairs' => 1,
                ],
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price_with_discount,
                'total_price' => round($quantity * $product->price_with_discount, 2),
            ],
        ]);
    }

    public function test_place_order_with_pair_discount_and_single_item(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()->withProductCategory()->withPairDiscount()->create();
        $table = Table::factory()->withOpenCashier()->create();
        $quantity = 3;
        $response = $this->post(route('orders.store'), [
            'table_id' => $table->id,
            'product_id' => $product->id,
            'quantity' => $quantity,
            'charge_service_fee' => true,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Order::get());

        $table->refresh();
        $this->assertEquals($table->summary, [
            [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'quantity' => $quantity,
                'detailedQuantity' => [
                    'single' => 1,
                    'pairs' => 1,
                ],
                'paid_quantity' => 0,
                'remaining_quantity' => $quantity,
                'charge_service_fee' => true,
                'unit_price' => $product->price_with_discount,
                'total_price' => round($quantity * $product->price_with_discount, 2),
            ],
        ]);
    }
}
