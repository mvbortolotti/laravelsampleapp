<?php

namespace Tests\Feature\Payments;

use App\Models\AccountEntry;
use App\Models\PaymentType;
use App\Models\Product;
use App\Models\Table;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakePaymentsByPriceTest extends TestCase
{
    use RefreshDatabase;

    public function test_payment_by_price(): void
    {
        $this->actingAs($user = User::factory()->create());

        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $order = $table->placeOrder(
            $product->id,
            1,
            true
        );
        $totalPrice = round($product->price + ($product->price * env('SERVICE_FEE', 0.1)), 2);
        $response = $this->post(route('payments.storeByPrice', ['table' => $order->table]), [
            'payment_type_id' => PaymentType::factory()->create()->id,
            'value' => $totalPrice,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $table->refresh();
        $this->assertCount(1, AccountEntry::get());
        $this->assertFalse($table->is_open);
    }

    public function test_partial_payment_by_price(): void
    {
        $this->actingAs($user = User::factory()->create());

        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $order = $table->placeOrder(
            $product->id,
            1,
            true
        );
        $totalPrice = round($product->price + ($product->price * env('SERVICE_FEE', 0.1)), 2);
        $paymentValue = round($totalPrice / 2, 2);
        $response = $this->post(route('payments.storeByPrice', ['table' => $order->table]), [
            'payment_type_id' => PaymentType::factory()->create()->id,
            'value' => $paymentValue,
        ]);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $table->refresh();
        $this->assertCount(1, AccountEntry::get());
        $this->assertTrue($table->is_open);
        $this->assertEquals($table->total - $paymentValue, $table->remaining_total);
    }

    public function test_payment_by_price_with_value_grather_than_total_bill(): void
    {
        $this->actingAs($user = User::factory()->create());

        $table = Table::factory()->withOpenCashier()->create();
        $product = Product::factory()->withProductCategory()->create();
        $order = $table->placeOrder(
            $product->id,
            1,
            true
        );
        $totalPrice = round($product->price + ($product->price * env('SERVICE_FEE', 0.1)), 2);

        $response = $this->post(route('payments.storeByPrice', ['table' => $order->table]), [
            'payment_type_id' => PaymentType::factory()->create()->id,
            'value' => $totalPrice + 1,
        ]);

        $response->assertRedirect();
        $response->assertSessionHasErrors();

        $table->refresh();
        $this->assertEquals($totalPrice, $table->remaining_total);
        $this->assertTrue($order->table->is_open);
    }
}
