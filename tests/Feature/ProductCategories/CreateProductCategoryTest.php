<?php

namespace Tests\Feature\ProductCategories;

use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateProductCategoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_product_categories_can_be_created(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productCategoryData = ProductCategory::factory()
            ->make()
            ->toArray();
        $response = $this->post(route('product-categories.store'), $productCategoryData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, ProductCategory::all());
    }

    public function test_product_categories_cannot_be_created_without_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productCategoryData = ProductCategory::factory()
            ->make(['name' => null])
            ->toArray();
        $response = $this->post(route('product-categories.store'), $productCategoryData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('name');
        $this->assertCount(0, ProductCategory::all());
    }
}
