<?php

namespace Tests\Feature\ProductCategories;

use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditProductCategoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_product_categories_can_be_updated_with_same_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productCategory = ProductCategory::factory()->create();

        $newProductCategoryData = ProductCategory::factory()
            ->make(['name' => $productCategory->name])
            ->toArray();

        $response = $this->post(route('product-categories.store', ['productCategory' => $productCategory]), $newProductCategoryData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $productCategory->refresh();
        $this->assertSame($newProductCategoryData['name'], $productCategory->name);
    }

    public function test_product_categories_can_be_updated(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productCategory = ProductCategory::factory()->create();

        $newProductCategoryData = ProductCategory::factory()
            ->make(['name' => $productCategory->name])
            ->toArray();

        $response = $this->post(route('product-categories.store', ['productCategory' => $productCategory]), $newProductCategoryData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $productCategory->refresh();
        $this->assertSame($newProductCategoryData['name'], $productCategory->name);
    }
}
