<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_products_can_be_created(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->make()
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Product::all());
        $product = Product::latest('id')->first();
        $this->assertFalse($product->has_pair_discount);
        $this->assertNull($product->price_with_discount);
    }

    public function test_products_can_be_created_with_pair_discount(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->withPairDiscount()
            ->make()
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $this->assertCount(1, Product::all());
        $product = Product::latest('id')->first();
        $this->assertTrue($product->has_pair_discount);
        $this->assertGreaterThan(0, $product->price_with_discount);
    }

    public function test_products_cannot_be_created_without_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->make(['name' => null])
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('name');
        $this->assertCount(0, Product::all());
    }

    public function test_products_cannot_be_created_with_long_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->make(['name' => Str::random(256)])
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('name');
        $this->assertCount(0, Product::all());
    }

    public function test_products_cannot_be_created_with_duplicated_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productName = 'Product Name';
        $product = Product::factory()
            ->withProductCategory()
            ->create(['name' => $productName]);

        $productData = Product::factory()
            ->withProductCategory()
            ->make(['name' => $productName])
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('name');
        $this->assertCount(1, Product::all());
    }

    public function test_products_cannot_be_created_with_negative_price(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->make(['price' => -1])
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('price');
        $this->assertCount(0, Product::all());
    }

    public function test_products_cannot_be_created_with_price_with_discount_greater_than_price(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()
            ->withProductCategory()
            ->withPairDiscount()
            ->make([
                'price' => 5,
                'price_with_discount' => 10,
            ])
            ->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('price_with_discount');
        $this->assertCount(0, Product::all());
    }

    public function test_products_cannot_be_created_without_product_category(): void
    {
        $this->actingAs($user = User::factory()->create());

        $productData = Product::factory()->make()->toArray();
        $response = $this->post(route('products.store'), $productData);

        $response->assertRedirect();
        $response->assertSessionHasErrors('product_category_id');
        $this->assertCount(0, Product::all());
    }
}
