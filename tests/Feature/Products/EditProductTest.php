<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Tests\TestCase;

class EditProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_products_can_be_edited(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()
            ->withProductCategory()
            ->create();

        $productCategory = ProductCategory::factory()->create();

        $newProductData = Product::factory()
            ->withProductCategory($productCategory)
            ->withPairDiscount()
            ->make()
            ->toArray();

        $response = $this->post(route('products.store', ['product' => $product]), $newProductData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $product->refresh();
        $this->assertSame(
            Arr::only($newProductData, ['name', 'price', 'has_pair_discount', 'price_with_discount', 'product_category_id']),
            Arr::only($product->toArray(), ['name', 'price', 'has_pair_discount', 'price_with_discount', 'product_category_id'])
        );
    }

    public function test_products_can_be_edited_with_same_name(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()
            ->withProductCategory()
            ->create();

        $productCategory = ProductCategory::factory()->create();

        $newProductData = Product::factory()
            ->withProductCategory($productCategory)
            ->withPairDiscount()
            ->make(['name' => $product->name])
            ->toArray();

        $response = $this->post(route('products.store', ['product' => $product]), $newProductData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $product->refresh();
        $this->assertSame($newProductData['name'], $product->name);
    }

    public function test_remove_pair_discount_from_product(): void
    {
        $this->actingAs($user = User::factory()->create());

        $product = Product::factory()
            ->withPairDiscount()
            ->withProductCategory()
            ->create();

        $productCategory = ProductCategory::factory()->create();

        $newProductData = Product::factory()
            ->withProductCategory($productCategory)
            ->make()
            ->toArray();

        $response = $this->post(route('products.store', ['product' => $product]), $newProductData);

        $response->assertRedirect();
        $response->assertSessionDoesntHaveErrors();

        $product->refresh();
        $this->assertSame(
            Arr::only($newProductData, ['name', 'price', 'has_pair_discount', 'price_with_discount', 'product_category_id']),
            Arr::only($product->toArray(), ['name', 'price', 'has_pair_discount', 'price_with_discount', 'product_category_id'])
        );
    }
}
